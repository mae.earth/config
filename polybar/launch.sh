#!/bin/sh

killall -q polybar

while pgrep -u mae -x polybar >/dev/null; do sleep 1; done

i3-msg restart

polybar -r topbar &
polybar -r topbar-2 &

